# Traffic Rule Collection

## Traffic Rules for Roads
The document [_traffic_rules.pdf_](https://gitlab.lrz.de/tum-cps/traffic-rules/-/blob/master/traffic_rules.pdf) is an extension to our published papers which are referenced below.

We will add more traffic rules soon.

**If you use our formalized traffic rules for research, please consider citing our papers:**
```
@inproceedings{Maierhofer2020,
	author = {Maierhofer, Sebastian and  Rettinger, Anna-Katharina and  Mayer, Eva Charlotte and  Althoff, Matthias},
	title = {Formalization of Interstate Traffic Rules in Temporal Logic},
	booktitle = {Proc. of the IEEE Intelligent Vehicles Symposium},
	year = {2020},
	pages     = {752-759},
	abstract = {To allow autonomous vehicles to safely participate in traffic and to avoid liability claims for car manufacturers, autonomous vehicles must obey traffic rules. However, current traffic rules are not formulated in a precise and mathematical way, so that they cannot be directly applied to autonomous vehicles. Additionally, several legal sources other than national traffic laws must be considered to infer detailed traffic rules. Thus, we formalize traffic rules for interstates based on the German Road Traffic Regulation, the Vienna Convention on Road Traffic, and legal decisions from courts. This makes it possible to automatically and unambiguously check whether traffic rules are being met by autonomous vehicles. Temporal logic is used to express the obtained rules mathematically. Our formalized traffic rules are evaluated for recorded data on more than 2,500 vehicles.},
}
@inproceedings{Maierhofer2022,
	author = {Maierhofer, Sebastian and  Moosbrugger, Paul and  Althoff, Matthias},
	title = {Formalization of Intersection Traffic Rules in Temporal Logic},
	booktitle = {Proc. of the IEEE Intelligent Vehicles Symposium},
	year = {2022},
	abstract = {To allow autonomous vehicles to safely participate in traffic and to avoid liability claims for car manufacturers, autonomous vehicles must obey traffic rules. However, current traffic rules are not formulated in a precise and mathematical way, so that they cannot be directly applied to autonomous vehicles. Additionally, several legal sources other than national traffic laws must be considered to infer detailed traffic rules. Thus, we formalize traffic rules for interstates based on the German Road Traffic Regulation, the Vienna Convention on Road Traffic, and legal decisions from courts. This makes it possible to automatically and unambiguously check whether traffic rules are being met by autonomous vehicles. Temporal logic is used to express the obtained rules mathematically. Our formalized traffic rules are evaluated for recorded data on more than 2,500 vehicles.},
```

## Marine Traffic Rules

We will soon provide additional formalized marine traffic rules.

**If you use our formalized marine traffic rules for research, please consider citing our paper:**
```
@inproceedings{Krasowski2021a,
	author = {Krasowski, Hanna and Althoff, Matthias},
	title = {Temporal Logic Formalization of Marine Traffic Rules},
	booktitle = {Proc. of the IEEE Intelligent Vehicles Symposium},
	year = {2021},
	abstract = {Autonomous vessels have to adhere to marine traffic rules to ensure traffic safety and reduce the liability
			of manufacturers. However, autonomous systems can only evaluate rule compliance if rules are formulated in a precise
			and mathematical way. This paper formalizes marine traffic rules from the Convention on the International Regulations
			for Preventing Collisions at Sea (COLREGS) using temporal logic. In particular, the collision prevention rules between
			two power-driven vessels are delineated. The formulation is based on modular predicates and adjustable parameters. We 
			evaluate the formalized rules in three US coastal areas for over 1,200 vessels using real marine traffic data.},
}
```
